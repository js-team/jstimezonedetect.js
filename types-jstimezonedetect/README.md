# Installation
> `npm install --save @types/jstimezonedetect`

# Summary
This package contains type definitions for jsTimezoneDetect (https://github.com/pellepim/jstimezonedetect).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/jstimezonedetect.

### Additional Details
 * Last updated: Tue, 27 Oct 2020 09:31:48 GMT
 * Dependencies: none
 * Global values: `jstz`

# Credits
These definitions were written by [Olivier Lamothe](https://github.com/olamothe).
